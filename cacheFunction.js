function cacheFunction(cb) {
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
    
    //How to check if cb is invoked or not?

    const cacheObject = {};

    const invokeCb = function(...arg){
        
        let cbReturn = cb(...arg);

        let currentArgs = arg;
        
        if(cacheObject[cbReturn] === undefined){
            cacheObject[cbReturn] = currentArgs;
            return cb(...arg);
        }else{
            let pastArgs = cacheObject[cbReturn];
            for(let i=0;i<pastArgs.length;i++){
                if(pastArgs[i]!==currentArgs[i]){
                    return cb(...arg);
                }
                return cbReturn;
            }
        }
        

    }

    return invokeCb;

}

module.exports = cacheFunction;