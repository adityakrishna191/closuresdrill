function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    
    let count = 0;

    const invokeCb = function(...arg){
        // console.log(cb, n);
        if (count < n) {
            count++;
            return cb(...arg);
        }else{
            return null;
        }
        
    }

    return invokeCb;

}

module.exports = limitFunctionCallCount;