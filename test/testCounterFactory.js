const counterFactory = require("../counterFactory")

const counter1 = counterFactory();
console.log(counter1.increment());
console.log(counter1.increment());
const counter2 = counterFactory();
console.log(counter2.decrement());
console.log(counter2.decrement());