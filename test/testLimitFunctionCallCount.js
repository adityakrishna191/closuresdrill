const limitFunctionCallCount = require("../limitFunctionCallCount");

// const add = (a,b) => a + b;

// const limitedAddFn = limitFunctionCallCount(add, 2);

// console.log(limitedAddFn(1,2)); // 3
// console.log(limitedAddFn(3,4)); // 7
// console.log(limitedAddFn(4,5)); // null

const doMath = (a,b,c) => a + b - c;

const limitedMathFn = limitFunctionCallCount(doMath, 3);

console.log(limitedMathFn(1,2,3)); // 0
console.log(limitedMathFn(3,4,5)); // 2
console.log(limitedMathFn(4,5,1)); // 8
console.log(limitedMathFn(4,5,6)); // null