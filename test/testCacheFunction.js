const cacheFunction = require("../cacheFunction.js");

const add = (a,b) => a + b;

const cachedAdd = cacheFunction(add);

console.log(cachedAdd(1,2)); // add should be invoked
console.log(cachedAdd(1,2)); // add should not be invoked but the same result returned from the cache
console.log(cachedAdd(2,1)); // add should be invoked
console.log(cachedAdd(2,3)); // add should be invoked
console.log(cachedAdd(2,3)); // add should not be invoked but the same result returned from the cache